package com.example.mobioticsencryption

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tool_bar.*

open class MainActivity : AppCompatActivity(), View.OnClickListener {
    protected var doubleBackPress: Boolean = false

    private val BACK_PRESS_DURATION = 1500L

    private val BACKPRESS_MSG = "Please press BACK again to exit!"
    override fun onClick(p0: View?) {
        if (p0?.id == R.id.tvBack) {
            container.visibility = View.GONE
            btn_container.visibility = View.VISIBLE
            setToolbarHeading(false, "CryptoString")
        } else {
            val manager = supportFragmentManager
            val ft = manager.beginTransaction()
            var fragment: EncryptDecryptFragment = EncryptDecryptFragment()
            var bundle: Bundle = Bundle()
            when (p0?.id) {
                R.id.encrypt -> {
                    bundle.putBoolean("IS_ENCRYPT", true)
                }
                R.id.decrypt -> {
                    bundle.putBoolean("IS_ENCRYPT", false)

                }
            }
            container.visibility = View.VISIBLE
            btn_container.visibility = View.GONE
            fragment.arguments = bundle
            ft.replace(R.id.container, fragment, fragment.javaClass.name)
            ft.commit()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        encrypt.setOnClickListener(this)
        decrypt.setOnClickListener(this)
        tvBack.setOnClickListener(this)
        setToolbarHeading(false, "CryptoString")
    }

    fun setToolbarHeading(isBackVisible: Boolean, header: String) {
        if (isBackVisible)
            tvBack.visibility = View.VISIBLE
        else tvBack.visibility = View.GONE
        tvHeader.text = header
    }

    override fun onBackPressed() {
        if (doubleBackPress) {
            super.onBackPressed()
            return
        }

        doubleBackPress = true
        Toast.makeText(this, BACKPRESS_MSG, Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackPress = false }, BACK_PRESS_DURATION)
    }

}
