package com.example.mobioticsencryption

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_encrypt_decrypt.*
import java.lang.StringBuilder

class EncryptDecryptFragment : Fragment() {
    private lateinit var etCrypto: EditText
    private lateinit var tvAnswer: TextView
    private var mHeader: String = ""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_encrypt_decrypt, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi();
    }

    private fun initUi() {
        if (view != null) {
            if (arguments != null && arguments!!.getBoolean("IS_ENCRYPT") != null) {
                if (arguments!!.getBoolean("IS_ENCRYPT")) {
                    mHeader = "Encrypt"
                } else mHeader = "Decrypt"
            }
            etCrypto = view!!.findViewById(R.id.etText);
            btnsubmit.isEnabled = false
            etCrypto.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {
                    btnsubmit.isEnabled = true
                }

                override fun beforeTextChanged(
                    s: CharSequence, start: Int,
                    count: Int, after: Int
                ) {
                }

                override fun onTextChanged(
                    s: CharSequence, start: Int,
                    before: Int, count: Int
                ) {
                    tvAnswer.text = ""
                }
            })
            tvAnswer = view!!.findViewById(R.id.tvAnswer);
            (activity as MainActivity).setToolbarHeading(true, mHeader)
            btnsubmit.setOnClickListener {
                convertString();
            }

        }
    }

    private fun convertString() {
        if (etCrypto.text != null && !etCrypto.text.isEmpty()) {
            var chars = etCrypto.text.toString().toCharArray()
            if (arguments!!.getBoolean("IS_ENCRYPT"))
                encryptString(chars)
            else
                decryptString(chars)
        }
    }

    private fun encryptString(chars: CharArray) {
        var ans = StringBuilder()
        var count = 1
        var temp = chars[0]
        for (i in 0 until chars.size) {
            if (temp == chars[i]) {
                if (i != 0)
                    count = count + 1
            } else {
                ans.append(temp)
                ans.append(count)
                temp = chars[i]
                count = 1
            }
        }
        ans.append(chars[chars.size - 1])
        ans.append(count)
        tvAnswer.text = ans
    }

    private fun decryptString(chars: CharArray) {
        var ans = StringBuilder()
        var prev = chars[0]
        for (i in 0 until chars.size) {
            if (chars[i].isDigit()) {
                var count: Int = Character.getNumericValue(chars[i])
                while (count > 0) {
                    ans.append(prev)
                    count -= 1;
                }
            } else {
                prev = chars[i]
            }
        }
        tvAnswer.text = ans
    }
}
